import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing
import re
import os

process = cms.Process("PandaNtupler")
cmssw_base = os.environ['CMSSW_BASE']


options = VarParsing.VarParsing ('analysis')
options.register('isData',
        True,
        VarParsing.VarParsing.multiplicity.singleton,
        VarParsing.VarParsing.varType.bool,
        "True if running on Data, False if running on MC")

options.register('isGrid', False, VarParsing.VarParsing.multiplicity.singleton,VarParsing.VarParsing.varType.bool,"Set it to true if running on Grid")

options.parseArguments()
isData = options.isData


process.load("FWCore.MessageService.MessageLogger_cfi")
# If you run over many samples and you save the log, remember to reduce
# the size of the output by prescaling the report of the event number
process.MessageLogger.cerr.FwkReport.reportEvery = 1000

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

if isData:
   fileList = [
       'file:/tmp/mcremone/2CF02CDC-D819-E611-AA68-02163E011A52.root'
       ]
else:
   fileList = [
#       'file:/tmp/snarayan/miniaod_ttdm.root'
       'file:/tmp/mcremone/30F60D71-0626-E611-ADA0-003048F5ADEC.root'
       ]
### do not remove the line below!
###FILELIST###

process.source = cms.Source("PoolSource",
        	skipEvents = cms.untracked.uint32(0),
        	fileNames = cms.untracked.vstring(fileList)
        )

# ---- define the output file -------------------------------------------
process.TFileService = cms.Service("TFileService",
          closeFileFast = cms.untracked.bool(True),
          fileName = cms.string("panda.root"),
        )

##----------------GLOBAL TAG ---------------------------
# used by photon id and jets
process.load("Configuration.Geometry.GeometryIdeal_cff") 
process.load('Configuration.StandardSequences.Services_cff')
process.load("Configuration.StandardSequences.MagneticField_cff")

#mc https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions#Global_Tags_for_Run2_MC_Producti
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
if (isData):
    # sept reprocessing
    process.GlobalTag.globaltag = '80X_dataRun2_2016SeptRepro_v3'
else:
    ## tranch IV v6 ... is this correct?
    process.GlobalTag.globaltag = '80X_mcRun2_asymptotic_2016_miniAODv2' # for 8011 MC? 
    # process.GlobalTag.globaltag = '80X_mcRun2_asymptotic_2016_TrancheIV_v6'

### LOAD DATABASE
from CondCore.DBCommon.CondDBSetup_cfi import *
#from CondCore.CondDB.CondDB_cfi import *

######## LUMI MASK
#if isData and not options.isGrid and False: ## dont load the lumiMaks, will be called by crab
if isData:
    import FWCore.PythonUtilities.LumiList as LumiList
    process.source.lumisToProcess = LumiList.LumiList(filename='Cert_271036-284044_13TeV_23Sep2016ReReco_Collisions16_JSON.txt').getVLuminosityBlockRange()
    print "Using local JSON"

### LOAD CONFIGURATION
process.load('PandaProd.Filter.infoProducerSequence_cff')
process.load('PandaProd.Filter.MonoXFilterSequence_cff')
process.load('PandaProd.Ntupler.PandaProd_cfi')

#-----------------------JES/JER----------------------------------
if isData:
  jeclabel = 'Spring16_25nsV6_DATA'
else:
  jeclabel = 'Spring16_25nsV6_MC'
process.jec =  cms.ESSource("PoolDBESSource",
                    CondDBSetup,
                    toGet = cms.VPSet(
              cms.PSet(record  = cms.string('JetCorrectionsRecord'),
                       tag     = cms.string('JetCorrectorParametersCollection_'+jeclabel+'_AK4PFPuppi'),
                       label   = cms.untracked.string('AK4Puppi')
                       ),
               cms.PSet(record  = cms.string('JetCorrectionsRecord'),
                        tag     = cms.string('JetCorrectorParametersCollection_'+jeclabel+'_AK8PFPuppi'),
                        label   = cms.untracked.string('AK8Puppi')
                        ),
              cms.PSet(record  = cms.string('JetCorrectionsRecord'),
                       tag     = cms.string('JetCorrectorParametersCollection_'+jeclabel+'_AK4PFchs'),
                       label   = cms.untracked.string('AK4chs')
                       ),
              cms.PSet(record  = cms.string('JetCorrectionsRecord'),
                       tag     = cms.string('JetCorrectorParametersCollection_'+jeclabel+'_AK8PFchs'),
                       label   = cms.untracked.string('AK8chs')
                       ),
              cms.PSet(record  = cms.string('JetCorrectionsRecord'),
                       tag     = cms.string('JetCorrectorParametersCollection_'+jeclabel+'_AK4PF'),
                       label   = cms.untracked.string('AK4')
                       ),
               cms.PSet(record  = cms.string('JetCorrectionsRecord'),
                        tag     = cms.string('JetCorrectorParametersCollection_'+jeclabel+'_AK8PF'),
                        label   = cms.untracked.string('AK8')
                        )
               ),

        )  
process.jec.connect = cms.string('sqlite:jec/%s.db'%jeclabel)
process.es_prefer_jec = cms.ESPrefer('PoolDBESSource', 'jec')

if isData:
  jerlabel = 'Spring16_25nsV6_DATA'
else:
  jerlabel = 'Spring16_25nsV6_MC'
process.jer = cms.ESSource("PoolDBESSource",
                  CondDBSetup,
                  toGet = cms.VPSet(
              cms.PSet(record  = cms.string('JetResolutionRcd'),
                       tag     = cms.string('JR_%s_PtResolution_AK4PFchs'%jerlabel),
                       label   = cms.untracked.string('AK4PFchs_pt'),
                      ),
              cms.PSet(record  = cms.string('JetResolutionRcd'),
                       tag     = cms.string('JR_%s_PhiResolution_AK4PFchs'%jerlabel),
                       label   = cms.untracked.string('AK4PFchs_phi'),
                      ),
              cms.PSet(record  = cms.string('JetResolutionScaleFactorRcd'),
                       tag     = cms.string('JR_%s_SF_AK4PFchs'%jerlabel),
                       label   = cms.untracked.string('AK4PFchs'),
                      ),
              cms.PSet(record  = cms.string('JetResolutionRcd'),
                       tag     = cms.string('JR_%s_PtResolution_AK4PFPuppi'%jerlabel),
                       label   = cms.untracked.string('AK4PFPuppi_pt'),
                      ),
              cms.PSet(record  = cms.string('JetResolutionRcd'),
                       tag     = cms.string('JR_%s_PhiResolution_AK4PFPuppi'%jerlabel),
                       label   = cms.untracked.string('AK4PFPuppi_phi'),
                      ),
              cms.PSet(record  = cms.string('JetResolutionScaleFactorRcd'),
                       tag     = cms.string('JR_%s_SF_AK4PFPuppi'%jerlabel),
                       label   = cms.untracked.string('AK4PFPuppi'),
                      ),
             )
        )
process.jer.connect = cms.string('sqlite:jer/%s.db'%jerlabel)
process.es_prefer_jer = cms.ESPrefer('PoolDBESSource', 'jer')


#-----------------------ELECTRON ID-------------------------------
from PandaProd.Ntupler.egammavid_cfi import *

initEGammaVID(process,options)

### ##ISO
process.load("RecoEgamma/PhotonIdentification/PhotonIDValueMapProducer_cfi")
process.load("RecoEgamma/ElectronIdentification/ElectronIDValueMapProducer_cfi")

#### RECOMPUTE JEC From GT ###
from PhysicsTools.PatAlgos.tools.jetTools import updateJetCollection
 
jecLevels= ['L1FastJet',  'L2Relative', 'L3Absolute']
if options.isData:
        jecLevels.append('L2L3Residual')
 
updateJetCollection(
    process,
    jetSource = process.PandaNtupler.chsAK4,
    labelName = 'UpdatedJEC',
    jetCorrections = ('AK4PFchs', cms.vstring(jecLevels), 'None')  
)

process.PandaNtupler.chsAK4=cms.InputTag('updatedPatJetsUpdatedJEC') # replace CHS with updated JEC-corrected

process.jecSequence = cms.Sequence( process.patJetCorrFactorsUpdatedJEC* process.updatedPatJetsUpdatedJEC)

########### MET Filter ################
process.load('RecoMET.METFilters.BadPFMuonFilter_cfi')
process.BadPFMuonFilter.muons = cms.InputTag("slimmedMuons")
process.BadPFMuonFilter.PFCandidates = cms.InputTag("packedPFCandidates")
process.BadPFMuonFilter.taggingMode = cms.bool(True)

process.load('RecoMET.METFilters.BadChargedCandidateFilter_cfi')
process.BadChargedCandidateFilter.muons = cms.InputTag("slimmedMuons")
process.BadChargedCandidateFilter.PFCandidates = cms.InputTag("packedPFCandidates")
process.BadChargedCandidateFilter.taggingMode = cms.bool(True)

process.metfilterSequence = cms.Sequence(process.BadPFMuonFilter
                                          *process.BadChargedCandidateFilter)

if not options.isData:
  process.PandaNtupler.metfilter = cms.InputTag('TriggerResults','','PAT')

############ RECOMPUTE PUPPI/MET #######################
from PhysicsTools.PatUtils.tools.runMETCorrectionsAndUncertainties import runMetCorAndUncFromMiniAOD
from PhysicsTools.PatAlgos.slimming.puppiForMET_cff import makePuppiesFromMiniAOD

makePuppiesFromMiniAOD(process,True)
process.puppi.useExistingWeights = False # I still don't trust miniaod...
process.puppiNoLep.useExistingWeights = False

runMetCorAndUncFromMiniAOD(process,         ## PF MET
                            isData=isData)
runMetCorAndUncFromMiniAOD(process,         ## Puppi MET
                            isData=options.isData,
                            metType="Puppi",
                            pfCandColl=cms.InputTag("puppiForMET"),
                            recoMetFromPFCs=True,
                            jetFlavor="AK4PFPuppi",
                            postfix="Puppi")
process.PandaNtupler.pfmet = cms.InputTag('slimmedMETs','','PandaNtupler')
process.PandaNtupler.puppimet = cms.InputTag('slimmedMETsPuppi','','PandaNtupler')
process.MonoXFilter.met = cms.InputTag('slimmedMETs','','PandaNtupler')
process.MonoXFilter.puppimet = cms.InputTag('slimmedMETsPuppi','','PandaNtupler')

############ RUN CLUSTERING ##########################
process.jetSequence = cms.Sequence()

# btag and patify puppi AK4 jets
from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
from PhysicsTools.PatAlgos.tools.pfTools import *

if not isData:
    process.packedGenParticlesForJetsNoNu = cms.EDFilter("CandPtrSelector", 
      src = cms.InputTag("packedGenParticles"), 
      cut = cms.string("abs(pdgId) != 12 && abs(pdgId) != 14 && abs(pdgId) != 16")
    )
    process.ak4GenJetsNoNu = ak4GenJets.clone(src = 'packedGenParticlesForJetsNoNu')
    process.jetSequence += process.packedGenParticlesForJetsNoNu
    process.jetSequence += process.ak4GenJetsNoNu

# btag and patify jets for access later
addJetCollection(
  process,
  labelName = 'PFAK4Puppi',
  jetSource=cms.InputTag('ak4PFJetsPuppi'), # this is constructed in runMetCorAndUncFromMiniAOD
  algo='AK4',
  rParam=0.4,
  pfCandidates = cms.InputTag("puppiForMET"),
  pvSource = cms.InputTag('offlineSlimmedPrimaryVertices'),
  svSource = cms.InputTag('slimmedSecondaryVertices'),
  muSource = cms.InputTag('slimmedMuons'),
  elSource = cms.InputTag('slimmedElectrons'),
  btagInfos = [
      'pfImpactParameterTagInfos'
     ,'pfInclusiveSecondaryVertexFinderTagInfos'
  ],
  btagDiscriminators = [
     'pfCombinedInclusiveSecondaryVertexV2BJetTags'
  ],
  genJetCollection = cms.InputTag('ak4GenJetsNoNu'),
  genParticles = cms.InputTag('prunedGenParticles'),
  getJetMCFlavour = False, # jet flavor disabled
)

if not isData:
  process.jetSequence += process.patJetPartonMatchPFAK4Puppi
  process.jetSequence += process.patJetGenJetMatchPFAK4Puppi
process.jetSequence += process.pfImpactParameterTagInfosPFAK4Puppi
process.jetSequence += process.pfInclusiveSecondaryVertexFinderTagInfosPFAK4Puppi
process.jetSequence += process.pfCombinedInclusiveSecondaryVertexV2BJetTagsPFAK4Puppi
process.jetSequence += process.patJetsPFAK4Puppi

<<<<<<< HEAD
        )  
if isData:
  process.jec.connect = cms.string('sqlite:jec/Spring16_25nsV6_DATA.db')
else:
  process.jec.connect = cms.string('sqlite:jec/Spring16_25nsV6_MC.db')
process.es_prefer_jec = cms.ESPrefer('PoolDBESSource', 'jec')

from JetMETCorrections.Configuration.JetCorrectorsAllAlgos_cff  import *
jetlabel='AK4PFPuppi'
process.ak4PuppiL1  = ak4PFCHSL1FastjetCorrector.clone (algorithm = cms.string(jetlabel))
process.ak4PuppiL2  = ak4PFCHSL2RelativeCorrector.clone(algorithm = cms.string(jetlabel))
process.ak4PuppiL3  = ak4PFCHSL3AbsoluteCorrector.clone(algorithm = cms.string(jetlabel))
process.ak4PuppiRes = ak4PFCHSResidualCorrector.clone  (algorithm = cms.string(jetlabel))
process.puppiJetMETSequence += process.ak4PuppiL1
process.puppiJetMETSequence += process.ak4PuppiL2
process.puppiJetMETSequence += process.ak4PuppiL3

process.ak4PuppiCorrector = ak4PFL1FastL2L3Corrector.clone(
        correctors = cms.VInputTag("ak4PuppiL1", 
                                    "ak4PuppiL2",
                                    "ak4PuppiL3")
    )
process.ak4PuppiCorrectorRes = ak4PFL1FastL2L3Corrector.clone(
        correctors = cms.VInputTag("ak4PuppiL1", 
                                    "ak4PuppiL2",
                                    "ak4PuppiL3",
                                    'ak4PuppiRes')
    )
if isData:
    process.puppiJetMETSequence += process.ak4PuppiRes
    process.puppiJetMETSequence += process.ak4PuppiCorrectorRes
    correctorLabel = 'ak4PuppiCorrectorRes'
else:
    process.puppiJetMETSequence += process.ak4PuppiCorrector
    correctorLabel = 'ak4PuppiCorrector'

# correct puppi MET
process.puppiMETcorr = cms.EDProducer("PFJetMETcorrInputProducer",
    src = cms.InputTag('ak4PFJetsPuppi'),
    offsetCorrLabel = cms.InputTag('ak4PuppiL1'),
    jetCorrLabel = cms.InputTag(correctorLabel),
    jetCorrLabelRes = cms.InputTag('ak4PuppiCorrectorRes'),
    jetCorrEtaMax = cms.double(9.9),
    type1JetPtThreshold = cms.double(15.0),
    skipEM = cms.bool(True),
    skipEMfractionThreshold = cms.double(0.90),
    skipMuons = cms.bool(True),
    skipMuonSelection = cms.string("isGlobalMuon | isStandAloneMuon")
)
process.type1PuppiMET = cms.EDProducer("CorrectedPFMETProducer",
    src = cms.InputTag('pfMETPuppi'),
    srcCorrections = cms.VInputTag(cms.InputTag('puppiMETcorr', 'type1')),
)   
process.puppiJetMETSequence += process.puppiMETcorr
process.puppiJetMETSequence += process.type1PuppiMET
=======
##################### FAT JETS #############################
>>>>>>> e36e2e225da92060452a75c8b3324f87c772ddfb

from PandaProd.Ntupler.makeFatJets_cff import *
fatjetInitSequence = initFatJets(process,isData)
process.jetSequence += fatjetInitSequence

if process.PandaNtupler.doCHSAK8:
  ak8CHSSequence    = makeFatJets(process,
                                  isData=isData,
                                  pfCandidates='pfCHS',
                                  algoLabel='AK',
                                  jetRadius=0.8)
  process.jetSequence += ak8CHSSequence
if process.PandaNtupler.doPuppiAK8:
  ak8PuppiSequence  = makeFatJets(process,
                                  isData=isData,
                                  pfCandidates='puppi',
                                  algoLabel='AK',
                                  jetRadius=0.8)
  process.jetSequence += ak8PuppiSequence
if process.PandaNtupler.doCHSCA15:
  ca15CHSSequence   = makeFatJets(process,
                                  isData=isData,
                                  pfCandidates='pfCHS',
                                  algoLabel='CA',
                                  jetRadius=1.5)
  process.jetSequence += ca15CHSSequence
if process.PandaNtupler.doPuppiCA15:
  ca15PuppiSequence = makeFatJets(process,
                                  isData=isData,
                                  pfCandidates='puppi',
                                  algoLabel='CA',
                                  jetRadius=1.5)
  process.jetSequence += ca15PuppiSequence

if not isData:
  process.ak4GenJetsYesNu = ak4GenJets.clone(src = 'packedGenParticles')
  process.jetSequence += process.ak4GenJetsYesNu

###############################

DEBUG=False
if DEBUG:
  print "Process=",process, process.__dict__.keys()

process.p = cms.Path(
                        process.infoProducerSequence *
                        process.jecSequence *
                        process.egmGsfElectronIDSequence *
                        process.egmPhotonIDSequence *
                        process.photonIDValueMapProducer *     # iso map for photons
                        process.electronIDValueMapProducer *   # iso map for photons
                        process.fullPatMetSequence *           # pf MET 
                        process.puppiMETSequence *             # builds all the puppi collections
                        process.egmPhotonIDSequence *          # baseline photon ID for puppi correction
                        process.fullPatMetSequencePuppi *      # puppi MET
                        process.monoXFilterSequence *          # filter
                        process.jetSequence *                  # patify ak4puppi and do all fatjet stuff
                        process.metfilterSequence *
                        process.PandaNtupler
                    )

if DEBUG:
  process.output = cms.OutputModule("PoolOutputModule",
                                    fileName = cms.untracked.string('pool.root'))
  process.output_step = cms.EndPath(process.output)

  process.schedule = cms.Schedule(process.p,
  		                            process.output_step)
